### Yojji

# Array implementation

It is **FORBIDDEN** to use a real `Array` in any form, rest/spread operators and classes like `Map, Set`

It is **FORBIDDEN** to google a real `Array` implementation or polyfills of methods

1. **Create** class `MyArray` by using `class` or `function`, it's up to you.

#### How it should work?

```
  const arr = new MyArray(1, 4, [2, 4], { name: 'Jack' });
  arr[0] === 1; // true
  arr[3].name === 'Jack'; // true
  arr.length === 4; // true
```

2. To `MyArray` you have to add next methods:
 - `push/pop` - add/remove element to the end of `MyArray` instance
 - `from` - creates an array from something iterable, for example, `arguments`
 - `map` - iterates over all elements of the array, applying a callback function(3 arguments as in the original) for every and return a new instance of `MyArray`
 - `forEach` - executes a provided function(3 arguments as in the original) once for each array element
 - `reduce` - executes a reducer function(4 arguments as in the original) on each element of the array, resulting in single output value
 - `filter` -  iterates over array and returns a new array instance with elements which were resolved as true against the conditional rule provided in callback function
 - `sort` - sorts the elements of an array in place and returns the sorted array
 - `toString` - this is the case when we override it :) will return a string like `"1,4,2,4, [object Object]"`

 3. And of course a challenging assignment:) I want to be able to do this:
   `const realArr = [...arr];`  // as a result - a real Array `[1 ,4, [2,4], { name: 'ivan' }]`


### Getting Started
1. go to [gitlab](https://gitlab.com) and create new repo <array_implementation>, it is necessary for step 5.
2. open terminal, go to folder with your progect.
(```cd ~/<your path to work folder>```)
3. clone repo by `ssh` (how to [create ssh](), how to [add ssh](https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account))
```sh
$ git clone git@gitlab.com:yojji-team/internship_2020/javascript/array_implementation.git
```
4. go to progect folder
```sh
$ cd array_implementation
```
5. add a new `remote` (take `url` from step 1, it is a link to you repo by `ssh`)
```sh
$ git remote add my_array <git@gitlab.com... path to repo on your gitlab>
```
6. use  `git push` to update your repo
```sh
$ git push my_array master
```
7. after finishing your task, create brunch <name.lasname> and push to origin repo
```sh
$ git push origin name.lasname
```